const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const blogTemplate = path.resolve("./src/templates/blog.js")
  const workTemplate = path.resolve("./src/templates/work.js")

  const blog = await graphql(`
    query {
      allContentfulBlog {
        edges {
          node {
            slug
          }
        }
      }
    }
  `)

  blog.data.allContentfulBlog.edges.forEach(edge => {
    createPage({
      component: blogTemplate,
      path: `/blogs/${edge.node.slug}`,
      context: {
        slug: edge.node.slug,
      },
    })
  })

  const work = await graphql(`
    query {
      allContentfulWork {
        edges {
          node {
            slug
          }
        }
      }
    }
  `)

  work.data.allContentfulWork.edges.forEach(edge => {
    createPage({
      component: workTemplate,
      path: `/works/${edge.node.slug}`,
      context: {
        slug: edge.node.slug,
      },
    })
  })
}

exports.onCreateWebpackConfig = ({ actions, stage }) => {
  if (stage === "build-javascript") {
    actions.setWebpackConfig({
      devtool: false,
    })
  }
}
