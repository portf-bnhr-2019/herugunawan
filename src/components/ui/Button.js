import styled from "styled-components"
import style from "styled-theming"
import { device } from "../../styles/device"

const Button = styled.div`
  padding: 2rem 2.2rem;
  font-size: 2rem;
  background-color: black;
  color: white;

  /* @media ${device.laptopL} {
    max-width: 114rem;
  }
  @media ${device.laptop} {
    max-width: 96rem;
  }
  @media ${device.tablet} {
    max-width: 72rem;
  }
  @media ${device.mobileL} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileM} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileS} {
    padding-left: 1.2rem;
    padding-right: 1.2rem;
  } */
`

export default Button
