import React from "react"

import styled from "styled-components"
import style from "styled-theming"
import Container from "./container/container"

import { device } from "../styles/device"

const loveColor = style("mode", {
  light: "#bb0000",
  dark: "#d0a9a9",
})

const Footers = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;
  padding: 3rem 0 2rem 0;
  margin-top: 3rem;

  p {
    font-size: 2rem;

    @media ${device.mobileL} {
      font-size: 1.6rem;
    }
    @media ${device.mobileM} {
      font-size: 1.6rem;
    }
    @media ${device.mobileS} {
      font-size: 1.4rem;
    }

    span {
      color: ${loveColor};
    }
  }
`

const Footer = () => (
  <Container>
    <Footers>
      <p>
        Made with <span>❤</span> by Heru Gunawan in 2019.
      </p>
    </Footers>
  </Container>
)

export default Footer
