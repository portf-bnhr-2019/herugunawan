import PropTypes from "prop-types"

import styled from "styled-components"

const Title = styled.h4`
  font-size: calc(2rem + (40 - 20) * ((100vw - 30rem) / (1600 - 300)));
  position: relative;
  margin-bottom: 3rem;
  letter-spacing: 0.18rem;
`

Title.propTypes = {
  variant: PropTypes.oneOf(["default", "primary"]),
}

Title.defaultProps = {
  variant: "default",
}

export default Title
