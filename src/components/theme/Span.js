import PropTypes from "prop-types"

import styled from "styled-components"
import theme from "styled-theming"

const textColor = theme.variants("mode", "variant", {
  default: { light: "#6c757d", dark: "#8d969e" },
  primary: { light: "yellow", dark: "green" },
})

const Span = styled.span`
  color: ${textColor};
`

Span.propTypes = {
  variant: PropTypes.oneOf(["default", "primary"]),
}

Span.defaultProps = {
  variant: "default",
}

export default Span
