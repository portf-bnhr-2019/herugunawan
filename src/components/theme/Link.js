import PropTypes from "prop-types"

import styled from "styled-components"
import theme from "styled-theming"

const textColor = theme.variants("mode", "variant", {
  default: { light: "#6c757d", dark: "#6fa587" },
  primary: { light: "yellow", dark: "green" },
})

const hoverColor = theme.variants("mode", "variant", {
  default: { light: "#229461", dark: "#17bf63" },
  primary: { light: "green", dark: "yellow" },
})

const bgColor = theme.variants("mode", "variant", {
  default: { light: "#f0fef8", dark: "transparent" },
  primary: { light: "green", dark: "yellow" },
})

const borderColor = theme.variants("mode", "variant", {
  default: { light: "#7d7d7d4a", dark: "transparent" },
  primary: { light: "green", dark: "yellow" },
})

const StyledLink = styled.a`
  color: ${textColor};
  border-bottom: 2px dotted ${borderColor};
  transition: background 0.2s ease, color 0.2s ease, border-bottom 0.2s ease;

  &:hover {
    background: ${bgColor};
    color: ${hoverColor};
    border-bottom: 2px dashed #39a559;
  }

  &:active {
    border-bottom: 2px dashed #39a559;
  }
`

StyledLink.propTypes = {
  variant: PropTypes.oneOf(["default", "primary"]),
}

StyledLink.defaultProps = {
  variant: "default",
}

export default StyledLink
