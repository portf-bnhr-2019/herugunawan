import PropTypes from "prop-types"

import { Link } from "gatsby"

import { device } from "../../styles/device"

import styled from "styled-components"
import theme from "styled-theming"

const textColor = theme.variants("mode", "variant", {
  default: { light: "#6c757d", dark: "#6fa587" },
  primary: { light: "yellow", dark: "green" },
})

const hoverColor = theme.variants("mode", "variant", {
  default: { light: "#21793d", dark: "rgb(20, 169, 87)" },
  primary: { light: "green", dark: "yellow" },
})

const MenuLink = styled(Link)`
  color: ${textColor};
  font-size: calc(1.6rem + (20 - 16) * ((100vw - 32rem) / (1600 - 320)));
  font-weight: 600;
  position: relative;
  padding-bottom: 0.7rem;

  &::after {
    content: "";
    display: block;
    width: 0;
    position: absolute;
    bottom: -0.2rem;
    left: 0;
    border-bottom: 0.2rem solid ${hoverColor};
    transition: width 0.5s ease-in-out;
  }

  &.active {
    color: ${hoverColor};
    padding-bottom: 0.7rem;
    border-bottom: 2px solid ${hoverColor};

    @media ${device.tablet} {
      padding-bottom: 0.4rem;
    }

    @media ${device.mobileL} {
      padding-bottom: 0.4rem;
    }

    @media ${device.mobileM} {
      padding-bottom: 0.4rem;
    }

    @media ${device.mobileS} {
      padding-bottom: 0.4rem;
    }
  }

  @media ${device.tablet} {
    padding-bottom: 0;
  }

  @media ${device.mobileL} {
    padding-bottom: 0;
  }

  @media ${device.mobileM} {
    padding-bottom: 0;
  }

  @media ${device.mobileS} {
    padding-bottom: 0;
    font-size: 1.4rem;
  }

  &:hover {
    color: ${hoverColor};

    &::after {
      width: 100%;
    }
  }
`

MenuLink.propTypes = {
  variant: PropTypes.oneOf(["default", "primary"]),
}

MenuLink.defaultProps = {
  variant: "default",
}

export default MenuLink
