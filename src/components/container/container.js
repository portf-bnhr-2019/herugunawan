import styled from "styled-components"
import { device } from "../../styles/device"

const Container = styled.div`
  padding-right: 1.5rem;
  padding-left: 1.5rem;
  margin-left: auto;
  margin-right: auto;
  width: 100%;

  @media ${device.laptopL} {
    max-width: 114rem;
  }
  @media ${device.laptop} {
    max-width: 96rem;
  }
  @media ${device.tablet} {
    max-width: 72rem;
  }
  @media ${device.mobileL} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileM} {
    padding-left: 2rem;
    padding-right: 2rem;
  }
  @media ${device.mobileS} {
    padding-left: 1.2rem;
    padding-right: 1.2rem;
  }
`

export default Container
