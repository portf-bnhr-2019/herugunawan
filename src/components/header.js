import React from "react"
import { Link } from "gatsby"

import styled from "styled-components"
import style from "styled-theming"
import { device } from "../styles/device"
import Container from "./container/container"

import ToggleMode from "./toggle/toggle"

import MenuLink from "./theme/StyledLink"

import logog from "../images/small.png"

const getBackground = style("mode", {
  light: "#fff",
  dark: "rgb(21, 32, 43)",
})

const Stick = styled.div`
  position: fixed;
  background-color: ${getBackground};
  left: 0;
  top: 0;
  z-index: 100;
  width: 100%;
  transition: all 0.3s linear;
`

const Headers = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-top: 2rem;

  @media ${device.tablet} {
    margin-top: 2rem;
    padding-top: 0;
  }

  @media ${device.mobileL} {
    margin-top: 2rem;
    padding-top: 0;
  }

  @media ${device.mobileM} {
    margin-top: 2rem;
    padding-top: 0;
  }

  @media ${device.mobileS} {
    margin-top: 2rem;
    padding-top: 0;
    display: flex;
    justify-content: space-between;
  }

  div {
    img {
      max-width: 6rem;
      margin: 2rem 0;

      @media ${device.mobileL} {
        max-width: 5rem;
      }

      @media ${device.mobileM} {
        max-width: 3.2rem;
      }

      @media ${device.mobileS} {
        max-width: 3rem;
      }
    }
  }
`

const Menu = styled.ul`
  display: flex;
  align-items: center;
  justify-content: space-between;

  li {
    margin-right: calc(0.7rem + (40 - 7) * ((100vw - 30rem) / (1600 - 300)));

    @media ${device.mobileL} {
      margin-right: 1.2rem;
    }

    @media ${device.mobileM} {
      margin-right: 1rem;
    }

    @media ${device.mobileS} {
      margin-right: 0.7rem;
    }
  }

  li:last-child {
    margin-right: 0;
  }
`

const Header = () => (
  <Stick>
    <Container>
      <Headers>
        <div>
          <Link to="/" title="Home">
            <img src={logog} alt="Logo Heru Gunawan" title="Back to home" />
          </Link>
        </div>
        <Menu>
          <li>
            <MenuLink to="/" activeClassName="active">
              Home
            </MenuLink>
          </li>

          <li>
            <MenuLink to="/about" activeClassName="active">
              About
            </MenuLink>
          </li>

          <li>
            <MenuLink to="/works" activeClassName="active">
              Work
            </MenuLink>
          </li>

          <li>
            <MenuLink to="/blogs" activeClassName="active">
              Blog
            </MenuLink>
          </li>

          <li>
            <MenuLink to="/contact" activeClassName="active">
              Contact
            </MenuLink>
          </li>

          <li>
            <ToggleMode aria="hideen" />
          </li>
        </Menu>
      </Headers>
    </Container>
  </Stick>
)

export default Header
