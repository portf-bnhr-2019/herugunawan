import React from "react"
import { Location } from "@reach/router"
import PropTypes from "prop-types"
import Helmet from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"
import { SchemaOrg } from "./SchemaOrg"

function SEO({ description, lang, meta, title, image, typePost }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            keywords
            author
            twitterUsername
            url
            siteUrl
            image
            organization {
              name
              url
              logo
            }
          }
        }
      }
    `
  )

  const defaults = site.siteMetadata
  const organ = site.siteMetadata.organization

  if (defaults.siteUrl === "" && typeof window !== "undefined") {
    defaults.siteUrl = window.location.origin
  }

  if (defaults.siteUrl === "") {
    console.error("Please set a siteUrl in your site metadata!")
    return null
  }

  const metaDescription = description || defaults.description
  const metaImage = `${image || defaults.image}`
  const typeOfPost = `${typePost || "website"}`
  const urlRoot = `${defaults.siteUrl}`
  const datePublished = false

  return (
    <Location>
      {({ location }) => {
        const url = `${urlRoot}${location.pathname}`

        return (
          <>
            <Helmet
              htmlAttributes={{
                lang,
              }}
              title={title}
              titleTemplate={`%s ~ ${defaults.title}`}
              meta={[
                {
                  name: `description`,
                  content: metaDescription,
                },
                {
                  name: `keywords`,
                  content: defaults.keywords,
                },
                {
                  property: `og:type`,
                  content: typeOfPost,
                },
                {
                  property: `og:url`,
                  content: url,
                },
                {
                  property: `og:title`,
                  content: title,
                },
                {
                  property: `og:description`,
                  content: metaDescription,
                },
                {
                  property: `og:site_name`,
                  content: `Heru Gunawan`,
                },
                {
                  property: `og:image`,
                  content: metaImage,
                },
                {
                  property: `fb:app_id`,
                  content: `440142346629990`,
                },
                {
                  name: `twitter:card`,
                  content: `summary_large_image`,
                },
                {
                  name: `twitter:site`,
                  content: defaults.twitterUsername,
                },
                {
                  name: `twitter:creator`,
                  content: defaults.twitterUsername,
                },
                {
                  property: `twitter:url`,
                  content: url,
                },
                {
                  name: `twitter:title`,
                  content: title,
                },
                {
                  name: `twitter:description`,
                  content: metaDescription,
                },
                {
                  name: `twitter:image`,
                  content: metaImage,
                },
                {
                  name: `google-site-verification`,
                  content: `g4qlczL9VWR5bdwuOZ3DXgRqvti9T-6J1AOXIowjkKM`,
                },
              ].concat(meta)}
            />
            <SchemaOrg
              isBlogPost={typeOfPost}
              url={url}
              title={title}
              image={image}
              description={description}
              canonicalUrl={url}
              defaultTitle="Heru Gunawan"
              organization={organ.name}
              datePublished={datePublished}
            />
          </>
        )
      }}
    </Location>
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default SEO
