import styled from "styled-components"

import { device } from "../../styles/device"

const Title = styled.div`
  margin: 8rem 0;

  @media ${device.mobileL} {
    padding: 0;
    margin: 7rem 0;
  }
  @media ${device.mobileM} {
    padding: 0;
    margin: 7rem 0;
  }
  @media ${device.mobileS} {
    padding: 0;
    margin: 7rem 0;
  }

  h1 {
    font-weight: bold;
    text-transform: capitalize;
    margin-bottom: calc(1.5rem + ((8 * (100vw - 36rem)) / 1560));
    font-size: calc(3rem + ((65 * (100vw - 36rem)) / 1420));
  }

  h2 {
    font-weight: 500;
    text-transform: capitalize;
    font-size: calc(1.6rem + ((10 * (100vw - 36rem)) / 1420));
    text-align: right;
    max-width: 80rem;
    margin: 0 auto;

    @media ${device.mobileL} {
      text-align: left;
    }
    @media ${device.mobileM} {
      text-align: left;
    }
    @media ${device.mobileS} {
      text-align: left;
    }
  }
`

export default Title
