import React from "react"
import PropTypes from "prop-types"

import Header from "./header"
import Container from "./container/container"
import Footer from "./footer"

import GlobalStyle from "../styles/globalStyles"

import styled, { ThemeProvider } from "styled-components"
import useTheme from "./theme/useTheme"
import { device } from "../styles/device"

const Top = styled.main`
  margin-top: 20rem;

  @media ${device.mobileL} {
    margin-top: 18rem;
  }
  @media ${device.mobileM} {
    margin-top: 16rem;
  }
  @media ${device.mobileS} {
    margin-top: 16rem;
  }
`

const Layout = ({ children }) => {
  const theme = useTheme()

  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <div className="all">
          <div className="wrapper">
            <Header />
            <Container>
              <Top>{children}</Top>
            </Container>
          </div>
          <Footer />
        </div>
      </>
    </ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
