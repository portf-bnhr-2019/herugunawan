import styled from "styled-components"
import style from "styled-theming"

const getBackground = style.variants("mode", "variant", {
  normal: {
    light: "#eee",
    dark: "rgb(21, 32, 43)",
  },
  primary: {
    light: "#fcc21b",
    dark: "#e0c820",
  },
})

const Button = styled.button`
  border: 0;
  background-color: transparent;
  fill: ${getBackground};
  display: flex;
  justify-content: center;
  align-items: center;
`

export default Button
