import React, { Fragment } from "react"

import { graphql } from "gatsby"
import Img from "gatsby-image"

import Layout from "../components/layout"
import SEO from "../components/seo"

import { device } from "../styles/device"

import styled from "styled-components"
import style from "styled-theming"

const textColor = style("mode", {
  light: "#6c757d",
  dark: "#6fa587",
})

const headingColor = style("mode", {
  light: "#505050",
  dark: "#d8dcd9",
})

const hoverColor = style("mode", {
  light: "#229461",
  dark: "#17bf63",
})

const bgColor = style("mode", {
  light: "#f0fef8",
  dark: "transparent",
})

const borderColor = style("mode", {
  light: "#7d7d7d4a",
  dark: "transparent",
})

const Title = styled.div`
  margin: 10rem 0;

  @media ${device.mobileL} {
    padding: 0;
    margin: 7rem 0;
  }
  @media ${device.mobileM} {
    padding: 0;
    margin: 7rem 0;
  }
  @media ${device.mobileS} {
    padding: 0;
    margin: 7rem 0;
  }

  h1 {
    font-weight: bold;
    text-transform: capitalize;
    margin-bottom: calc(1.5rem + ((8 * (100vw - 36rem)) / 1560));
    font-size: calc(2.8rem + (64 - 28) * ((100vw - 30rem) / (1600 - 300)));
  }

  h2 {
    font-weight: 500;
    color: ${headingColor};
    font-style: italic;
    font-size: calc(2rem + (30 - 20) * ((100vw - 30rem) / (1600 - 300)));
    margin-bottom: 3rem;
  }
`

const WorkImage = styled.div`
  margin-bottom: calc(4rem + (60 - 40) * ((100vw - 30rem) / (1600 - 300)));
  box-shadow: 0 30px 60px -10px rgba(0, 0, 0, 0.2),
    0 18px 36px -18px rgba(0, 0, 0, 0.22);

  img {
    border-radius: 5px;
  }
`

const Begin = styled.section`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(40rem, 1fr));
  grid-gap: 2rem;
  margin-bottom: calc(4rem + (60 - 40) * ((100vw - 30rem) / (1600 - 300)));

  @media ${device.mobileL} {
    grid-gap: 2rem;
    grid-template-columns: repeat(auto-fit, minmax(30rem, 1fr));
    grid-auto-rows: minmax(12rem, auto);
  }
  @media ${device.mobileM} {
    grid-gap: 2rem;
    grid-template-columns: repeat(auto-fit, minmax(20rem, 1fr));
    grid-auto-rows: minmax(12rem, auto);
  }
  @media ${device.mobileS} {
    grid-gap: 2rem;
    grid-template-columns: repeat(auto-fit, minmax(20rem, 1fr));
    grid-auto-rows: minmax(12rem, auto);
  }
`

const Intro = styled.div`
  margin-bottom: calc(4rem + (60 - 40) * ((100vw - 30rem) / (1600 - 300)));
  margin-top: calc(2rem + (40 - 20) * ((100vw - 30rem) / (1600 - 300)));

  h3 {
    font-size: calc(2.2rem + (32 - 22) * ((100vw - 30rem) / (1600 - 300)));
    margin-bottom: calc(1.6rem + (20 - 16) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 700;
  }

  p {
    font-size: calc(1.6rem + (20 - 16) * ((100vw - 30rem) / (1600 - 300)));
    line-height: calc(3rem + (40 - 30) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 400;
    margin: 0 0 calc(1.6rem + (40 - 16) * ((100vw - 30rem) / (1600 - 300))) 0;
  }
`

const Problem = styled.div`
  h3 {
    font-size: calc(2.2rem + (32 - 22) * ((100vw - 30rem) / (1600 - 300)));
    margin-bottom: calc(1.6rem + (20 - 16) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 700;
  }

  p {
    font-size: calc(1.6rem + (20 - 16) * ((100vw - 30rem) / (1600 - 300)));
    line-height: calc(3rem + (40 - 30) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 400;
    margin: 0 0 calc(1.6rem + (40 - 16) * ((100vw - 30rem) / (1600 - 300))) 0;
  }

  a {
    font-size: calc(1.6rem + (20 - 16) * ((100vw - 30rem) / (1600 - 300)));
    line-height: calc(3rem + (40 - 30) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 400;
    margin: 0 0 calc(1.6rem + (40 - 16) * ((100vw - 30rem) / (1600 - 300))) 0;
    color: ${textColor};
    border-bottom: 2px solid ${borderColor};
    transition: background 0.2s ease, color 0.2s ease, border-bottom 0.2s ease;

    &:hover {
      background: ${bgColor};
      color: ${hoverColor};
      border-bottom: 2px dashed #39a559;
    }

    &:active {
      border-bottom: 2px dashed #39a559;
    }
  }
`

export const query = graphql`
  query($slug: String!) {
    contentfulWork(slug: { eq: $slug }) {
      title
      subtitle
      categori {
        title
      }
      image {
        title
        fluid(
          maxWidth: 1000
          background: "rgb:000000"
          resizingBehavior: SCALE
        ) {
          ...GatsbyContentfulFluid
        }
      }
      intro {
        intro
      }
      problem {
        problem
      }
      solution {
        solution
      }
      website
      dribbble
    }
  }
`

const WorkPage = ({ data }) => {
  const {
    title,
    subtitle,
    problem,
    solution,
    intro,
    categori,
    image,
    dribbble,
    website,
  } = data.contentfulWork

  return (
    <Layout>
      <SEO title={title} description={subtitle} />
      <Title>
        <h1>{title}</h1>
        <h2>{subtitle}</h2>
      </Title>

      <WorkImage>
        <Img alt={image.title} fluid={image.fluid} />
      </WorkImage>

      <Intro>
        <h3>overview.</h3>
        <p>{intro.intro}</p>
      </Intro>

      <Intro>
        <h3>type of work.</h3>
        <p>{categori.title}</p>
      </Intro>

      <Begin>
        <Problem>
          <h3>the problem.</h3>
          <p>{problem.problem}</p>
        </Problem>
        <Problem>
          <h3>my solution.</h3>
          <p>{solution.solution}</p>
        </Problem>
      </Begin>

      <Begin>
        <Problem>
          <h3>showcase.</h3>
          {dribbble ? (
            <Fragment>
              <p>
                For case study visit: <a href={dribbble}>{title}</a>{" "}
              </p>
            </Fragment>
          ) : (
            <p>No Case Study</p>
          )}
        </Problem>
        <Problem>
          <h3>website.</h3>
          {website ? (
            <Fragment>
              <p>
                Visit: <a href={website}>{title}</a>{" "}
              </p>
            </Fragment>
          ) : (
            <Fragment>
              <p>No Website available</p>
            </Fragment>
          )}
        </Problem>
      </Begin>
    </Layout>
  )
}

export default WorkPage
