import React, { Fragment } from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"

import Layout from "../components/layout"
import SEO from "../components/seo"

// import {
//   FacebookShareCount,
//   FacebookShareButton,
//   TwitterShareButton,
//   TelegramShareButton,
//   WhatsappShareButton,
//   FacebookIcon,
//   TwitterIcon,
//   TelegramIcon,
//   WhatsappIcon,
// } from "react-share"

import { formatDistance } from "date-fns"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"
import { BLOCKS } from "@contentful/rich-text-types"

import { device } from "../styles/device"

import styled from "styled-components"
import style from "styled-theming"

const textColor = style("mode", {
  light: "#6c757d",
  dark: "#6fa587",
})

const hoverColor = style("mode", {
  light: "#229461",
  dark: "#17bf63",
})

const bgColor = style("mode", {
  light: "#f0fef8",
  dark: "transparent",
})

const borderColor = style("mode", {
  light: "#7d7d7d4a",
  dark: "transparent",
})

const textContent = style("mode", {
  light: "#000",
  dark: "#d8deda",
})

const bgContent = style("mode", {
  light: "#4c504ff5",
  dark: "#fff",
})

const Title = styled.div`
  padding: 2rem 3rem;
  text-align: center;
  margin-bottom: calc(2rem + (50 - 20) * ((100vw - 30rem) / (1600 - 300)));

  h1 {
    font-weight: bold;
    text-transform: capitalize;
    margin-bottom: calc(1rem + (14 - 10) * ((100vw - 30rem) / (1600 - 300)));
    font-size: calc(2rem + (40 - 20) * ((100vw - 30rem) / (1600 - 300)));
  }

  h2 {
    font-weight: 500;
    font-style: italic;
    font-size: calc(2.2rem + (32 - 22) * ((100vw - 30rem) / (1600 - 300)));
    margin-bottom: calc(2rem + (30 - 20) * ((100vw - 30rem) / (1600 - 300)));
  }

  @media ${device.mobileL} {
    padding: 2rem 0;
  }

  @media ${device.mobileM} {
    padding: 2rem 0;
  }

  @media ${device.mobileS} {
    padding: 2rem 0;
  }
`

const ShareBox = styled.div`
  p {
    font-size: 2.2rem;
  }
`

const AdditionalContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;

  @media ${device.mobileL} {
    justify-content: space-between;
  }

  @media ${device.mobileM} {
    justify-content: space-between;
  }

  @media ${device.mobileS} {
    flex-direction: column;
    align-items: flex-start;
  }
`

const Additional = styled.div`
  display: flex;
  align-items: center;

  svg {
    margin-right: 1rem;
  }

  span,
  time {
    font-size: calc(1.4rem + (20 - 14) * ((100vw - 30rem) / (1600 - 300)));
  }

  @media ${device.mobileS} {
    &:first-child {
      margin-bottom: 1rem;
      justify-content: flex-start;
    }
  }
`

const BlogQuote = styled.blockquote`
  position: relative;
  max-width: 75%;
  margin: calc(2rem + (50 - 20) * ((100vw - 30rem) / (1600 - 300))) auto;
  padding: 0 1rem;

  &::before {
    content: "\\275D";
    margin: 0 auto;
    position: absolute;
    top: -0.4rem;
    left: -6.6rem;
    font-size: 7rem;
    opacity: 0.6;

    @media ${device.mobileL} {
      display: none;
    }
    @media ${device.mobileM} {
      display: none;
    }
    @media ${device.mobileS} {
      display: none;
    }
  }

  &::after {
    content: "\\275E";
    margin: 0 auto;
    position: absolute;
    bottom: -0.4rem;
    right: -6.6rem;
    font-size: 7rem;
    opacity: 0.6;

    @media ${device.mobileL} {
      display: none;
    }
    @media ${device.mobileM} {
      display: none;
    }
    @media ${device.mobileS} {
      display: none;
    }
  }

  p {
    font-size: calc(1.9rem + (22 - 19) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 400;
    margin-bottom: 1.7rem;
    line-height: 1.7;
    color: ${textContent};
  }

  cite {
    font-size: calc(1.9rem + (22 - 19) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 500;
    font-family: sans-serif;
    font-style: oblique;
    position: relative;
    display: block;
    margin-top: 1.5rem;
    padding-left: 3rem;
    color: ${textContent};

    &::before {
      content: "";
      background-color: ${bgContent};
      height: 1px;
      margin-bottom: auto;
      margin-top: auto;
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      width: 2rem;
    }
  }

  @media ${device.mobileL} {
    max-width: 100%;
    padding: 0;
    padding-left: 1rem;
    border-left: 2px solid ${bgContent};
  }

  @media ${device.mobileM} {
    max-width: 100%;
    padding: 0;
    padding-left: 1rem;
    border-left: 2px solid ${bgContent};
  }

  @media ${device.mobileS} {
    max-width: 100%;
    padding: 0;
    padding-left: 1rem;
    border-left: 1px solid ${bgContent};
  }
`

const Content = styled.article`
  max-width: 75%;
  margin: 2rem auto;
  padding: 0 1rem;

  p {
    font-size: calc(1.6rem + (20 - 16) * ((100vw - 30rem) / (1600 - 300)));
    line-height: calc(3rem + (40 - 30) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 400;
    margin: 0 0 calc(1.6rem + (40 - 16) * ((100vw - 30rem) / (1600 - 300))) 0;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin-bottom: 1.6rem;
    line-height: calc(3rem + (40 - 30) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: bolder;
  }

  a {
    color: ${textColor};
    border-bottom: 2px solid ${borderColor};
    transition: background 0.2s ease, color 0.2s ease, border-bottom 0.2s ease;

    &:hover {
      background: ${bgColor};
      color: ${hoverColor};
      border-bottom: 2px dashed #39a559;
    }

    &:active {
      border-bottom: 2px dashed #39a559;
    }
  }

  @media ${device.mobileL} {
    max-width: 100%;
    padding: 0;
  }

  @media ${device.mobileM} {
    max-width: 100%;
    padding: 0;
  }

  @media ${device.mobileS} {
    max-width: 100%;
    padding: 0;
  }
`

const BlogImage = styled.div`
  margin: 5rem -10rem;
  box-shadow: 0 30px 60px -10px rgba(0, 0, 0, 0.2),
    0 18px 36px -18px rgba(0, 0, 0, 0.22);

  img {
    border-radius: 5px;
  }

  @media ${device.laptop} {
    margin: 5rem -3rem;
  }

  @media ${device.tablet} {
    margin: 5rem 0;
  }

  @media ${device.mobileL} {
    margin: 5rem 0;
  }

  @media ${device.mobileM} {
    margin: 2em 0;
  }

  @media ${device.mobileS} {
    margin: 2rem 0;
  }
`

export const query = graphql`
  query($slug: String!) {
    contentfulBlog(slug: { eq: $slug }) {
      title
      subtitle
      categori {
        title
      }
      image {
        title
        file {
          url
        }
        fluid(
          maxWidth: 1000
          background: "rgb:000000"
          resizingBehavior: SCALE
        ) {
          ...GatsbyContentfulFluid_tracedSVG
        }
      }
      date
      content {
        json
      }
      quote {
        name
        quote
      }
    }
  }
`

const BlogPage = ({ data }) => {
  const {
    title,
    subtitle,
    categori,
    date,
    content,
    image,
    quote,
  } = data.contentfulBlog

  return (
    <Layout>
      <SEO
        title={title}
        description={subtitle}
        image={image.file.url}
        typePost="article"
        imageAlt={image.title}
      />

      <Title>
        <h1>{title}</h1>
        <h2>{subtitle}</h2>

        <AdditionalContainer>
          <Additional>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-archive"
            >
              <polyline points="21 8 21 21 3 21 3 8"></polyline>
              <rect x="1" y="3" width="22" height="5"></rect>
              <line x1="10" y1="12" x2="14" y2="12"></line>
            </svg>
            <span>{categori.title}</span>
          </Additional>
          <Additional>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-calendar"
            >
              <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
              <line x1="16" y1="2" x2="16" y2="6"></line>
              <line x1="8" y1="2" x2="8" y2="6"></line>
              <line x1="3" y1="10" x2="21" y2="10"></line>
            </svg>
            <time>
              {formatDistance(new Date(date), new Date(), {
                addSuffix: true,
              })}
            </time>
          </Additional>
        </AdditionalContainer>
      </Title>

      <BlogImage>
        <Img alt={image.title} fluid={image.fluid} />
      </BlogImage>

      <Content>
        {documentToReactComponents(content.json, {
          renderNode: {
            [BLOCKS.EMBEDDED_ASSET]: (node, children) => (
              <img
                src={node.data.target.fields.file["en-US"].url}
                alt={node.data.target.fields.title}
              />
            ),
          },
        })}
      </Content>

      {quote && (
        <Fragment>
          <BlogQuote>
            <p>{quote.quote}</p>
            <cite>{quote.name}</cite>
          </BlogQuote>
        </Fragment>
      )}

      <ShareBox>
        <p>Share this article to</p>
      </ShareBox>
    </Layout>
  )
}

export default BlogPage
