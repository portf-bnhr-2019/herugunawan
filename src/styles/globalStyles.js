import { createGlobalStyle } from "styled-components"
import style from "styled-theming"

const getBackground = style("mode", {
  light: "#fff",
  dark: "rgb(21, 32, 43)",
})

const getForeground = style("mode", {
  light: "rgb(21, 32, 43)",
  dark: "#fff",
})

const GlobalStyle = createGlobalStyle`

  html {
    line-height: 1.15;
    -webkit-text-size-adjust: 100%;
  }

  body {
    margin: 0;
  }

  main {
    display: block;
  }

  h1 {
    font-size: 2em;
    margin: 0.67em 0;
  }

  hr {
    box-sizing: content-box;
    height: 0;
    overflow: visible;
  }

  pre {
    font-family: monospace, monospace;
    font-size: 1em;
  }

  a {
    background-color: transparent;
  }

  abbr[title] {
    border-bottom: none;
    text-decoration: underline;
    text-decoration: underline dotted;
  }

  b,
  strong {
    font-weight: bolder;
  }

  code,
  kbd,
  samp {
    font-family: monospace, monospace;
    font-size: 1em;
  }

  small {
    font-size: 80%;
  }

  sub,
  sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline;
  }

  sub {
    bottom: -0.25em;
  }

  sup {
    top: -0.5em;
  }

  img {
    border-style: none;
  }

  button,
  input,
  optgroup,
  select,
  textarea {
    font-family: inherit;
    font-size: 100%;
    line-height: 1.15;
    margin: 0;
  }

  button,
  input {
    overflow: visible;
  }

  button,
  select {
    text-transform: none;
  }

  button,
  [type="button"],
  [type="reset"],
  [type="submit"] {
    outline: 0;
  }

  button::-moz-focus-inner,
  [type="button"]::-moz-focus-inner,
  [type="reset"]::-moz-focus-inner,
  [type="submit"]::-moz-focus-inner {
    border-style: none;
    padding: 0;
  }

  button:-moz-focusring,
  [type="button"]:-moz-focusring,
  [type="reset"]:-moz-focusring,
  [type="submit"]:-moz-focusring {
    outline: 1px dotted ButtonText;
  }

  fieldset {
    padding: 0.35em 0.75em 0.625em;
  }

  legend {
    box-sizing: border-box;
    color: inherit;
    display: table;
    max-width: 100%;
    padding: 0;
    white-space: normal;
  }

  progress {
    vertical-align: baseline;
  }

  textarea {
    overflow: auto;
  }

  [type="checkbox"],
  [type="radio"] {
    box-sizing: border-box;
    padding: 0;
  }


  [type="number"]::-webkit-inner-spin-button,
  [type="number"]::-webkit-outer-spin-button {
    height: auto;
  }

  [type="search"] {
    -webkit-appearance: textfield;
    outline-offset: -2px;
  }


  [type="search"]::-webkit-search-decoration {
    -webkit-appearance: none;
  }


  ::-webkit-file-upload-button {
    -webkit-appearance: button;
    font: inherit;
  }


  details {
    display: block;
  }


  summary {
    display: list-item;
  }

  template {
    display: none;
  }

  [hidden] {
    display: none;
  }

  :root {
    --color-white: #ffffff;
    --color-black: #000000;
    --color-primary: #229461;
    --color-secondary: #17bf63;
  }

  ::-moz-selection {
    color: var(--color-white);
    background: var(--color-primary);
  }

  ::selection {
    color: var(--color-white);
    background: var(--color-primary);
  }

  *,
  *::before,
  *::after {
    padding: 0;
    margin: 0;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  html {
    scroll-behavior: smooth;
  }

  html,
  body {
    height: 100%;
  }

  body {
    width: 100%;
    min-height: 100%;
    background-color: ${getBackground};
    color: ${getForeground};
    transition: all 0.3s linear;
  }

  .all {
    height: 100%;
    width: 100%;
    min-height: 100vh;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
  }

  .wrapper {
    max-width: 100%;
    -webkit-box-flex: 1;
    -ms-flex: 1 0 auto;
    flex: 1 0 auto;
  }

  img {
    height: auto;
    max-height: 100%;
    width: 100%;
    max-width: 100%;
    vertical-align: middle;
  }

  ul {
    list-style: none;
  }

  html {
    font-size: 62.5%;
  }

  body {
    font-family: 'Nunito', -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", sans-serif;
    font-size: 1.6rem;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: 'Nunito', sans-serif;
    font-weight: 600;
    line-height: 1.5;
  }

  h1 {
    font-size: 4rem;
  }

  h2 {
    font-size: 3.2rem;
  }

  h3 {
    font-size: 2.8rem;
  }

  h4 {
    font-size: 2.4rem;
  }

  h5 {
    font-size: 2rem;
  }

  h6 {
    font-size: 1.6rem;
  }

  p {
    font-family: 'Nunito', sans-serif;
    font-weight: 300;
    line-height: 1.6;
  }

  ::-moz-focus-inner {
    border: 0;
  }

  a {
    text-decoration: none;
  }

  a:hover {
    color: var(--color-primary);
  }

  a:focus:not(:hover) {
    outline: .2rem dashed var(--color-primary);
    outline-offset: 3px;
  }

  button:focus:not(:hover) {
    outline: .2rem dashed var(--color-primary);
    outline-offset: 3px;
  }
`
export default GlobalStyle
