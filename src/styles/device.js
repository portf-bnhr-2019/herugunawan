const size = {
  mobileS: "319px",
  mobileS1: "374px",
  mobileM: "375px",
  mobileM1: "424px",
  mobileL: "425px",
  mobileL1: "767px",
  tablet: "768px",
  tablet1: "991px",
  laptop: "992px",
  laptop1: "1399px",
  laptopL: "1440px",
  laptopL1: "2560px",
  desktop: "2559px",
}

export const device = {
  mobileS: `only screen and (min-width: ${size.mobileS}) and (max-width: ${size.mobileS1})`,
  mobileM: `only screen and (min-width: ${size.mobileM}) and (max-width: ${size.mobileM1})`,
  mobileL: `only screen and (min-width: ${size.mobileL}) and (max-width: ${size.mobileL1})`,
  tablet: `only screen and (min-width: ${size.tablet}) and (max-width: ${size.tablet1})`,
  laptop: `only screen and (min-width: ${size.laptop}) and (max-width: ${size.laptop1})`,
  laptopL: `only screen and (min-width: ${size.laptopL}) and (max-width: ${size.laptopL1})`,
  desktop: `only screen and (max-width: ${size.desktop})`,
}
