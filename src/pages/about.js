import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

import Profile from "../components/images/profile"

import Title from "../components/title/title"

import styled from "styled-components"

import { device } from "../styles/device"

const ProfileImage = styled.div`
  margin-bottom: calc(4.6rem + (136 - 46) * ((100vw - 30rem) / (1600 - 300)));
  box-shadow: 0 30px 60px -10px rgba(0, 0, 0, 0.2),
    0 18px 36px -18px rgba(0, 0, 0, 0.22);

  img {
    border-radius: 5px;
  }
`

const Me = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 75%;

  @media ${device.mobileL} {
    max-width: 90%;
  }

  @media ${device.mobileM} {
    max-width: 90%;
  }

  @media ${device.mobileS} {
    max-width: 90%;
  }

  h3 {
    margin-bottom: calc(2.3rem + (33 - 23) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 700;
  }

  p {
    font-size: calc(1.6rem + (20 - 16) * ((100vw - 30rem) / (1600 - 300)));
    line-height: calc(3rem + (40 - 30) * ((100vw - 30rem) / (1600 - 300)));
    font-weight: 400;
    margin-bottom: 2rem;
  }

  p:last-child {
    margin-bottom: 0rem;
  }

  ul {
    margin-bottom: 2rem;
    li {
      font-size: calc(1.6rem + (20 - 16) * ((100vw - 30rem) / (1600 - 300)));
      line-height: calc(3rem + (40 - 30) * ((100vw - 30rem) / (1600 - 300)));
      position: relative;
      padding-left: 2.3rem;

      &::before {
        position: absolute;
        left: 0;
        content: "\\00bb";
        top: -0.1rem;
        font-size: 2.2rem;
      }
    }
  }
`

const StandAlone = styled.div`
  margin-bottom: calc(4.2rem + (120 - 42) * ((100vw - 30rem) / (1600 - 300)));

  &:last-child {
    margin-bottom: 0;
  }
`

const AboutPage = () => {
  const title = `About`
  const desc = `Dig deeper into your Front End Developer Partner`
  return (
    <Layout>
      <SEO title={desc} description={desc} />{" "}
      <Title>
        <h1> {title} </h1> <h2> {desc} </h2>{" "}
      </Title>
      <ProfileImage>
        <Profile />
      </ProfileImage>
      <Me>
        <StandAlone>
          <h3> Can you feel the love tonight? </h3>{" "}
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam
            reprehenderit quibusdam neque eius pariatur eum ratione, corrupti,
            nisi ex eligendi quidem sequi repellendus dolor placeat aperiam
            magnam! Iste sunt sint repellat, vel non nihil alias consequatur
            expedita porro provident? Mollitia voluptas sit dolore adipisci
            reiciendis! Fuga laborum a laboriosam in?{" "}
          </p>{" "}
        </StandAlone>{" "}
      </Me>{" "}
    </Layout>
  )
}

export default AboutPage
