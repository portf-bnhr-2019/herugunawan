import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Title from "../components/title/title"

import StyledLink from "../components/theme/Link"

import styled from "styled-components"

import { device } from "../styles/device"

const Contact = styled.section`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(40rem, 1fr));
  grid-auto-rows: minmax(15rem, auto);
  grid-gap: 3rem;

  @media ${device.mobileL} {
    grid-gap: 2rem;
    grid-template-columns: repeat(auto-fit, minmax(30rem, 1fr));
    grid-auto-rows: minmax(12rem, auto);
  }
  @media ${device.mobileM} {
    grid-gap: 2rem;
    grid-template-columns: repeat(auto-fit, minmax(20rem, 1fr));
    grid-auto-rows: minmax(12rem, auto);
  }
  @media ${device.mobileS} {
    grid-gap: 2rem;
    grid-template-columns: repeat(auto-fit, minmax(20rem, 1fr));
    grid-auto-rows: minmax(12rem, auto);
  }
`

const Box = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border: 2px dashed #6c757d;

  h2 {
    font-size: calc(2.5rem + (25 - 18) * ((100vw - 30rem) / (1600 - 300)));
    color: #6c757d;
  }

  h3 {
    font-size: calc(1.9rem + (19 - 15) * ((100vw - 30rem) / (1600 - 300)));
  }

  div {
    a {
      font-size: calc(1.9rem + (19 - 15) * ((100vw - 30rem) / (1600 - 300)));
      margin-right: 1rem;
    }

    a:last-child {
      margin-right: 0rem;
    }
  }
`

const ContactPage = () => (
  <Layout>
    <SEO
      title="Get in touch for more details"
      description="Email, Phone, Address, or how to reach Heru Gunawan in Social media and Dev Things"
    />
    <Title>
      <h1>Contact Me</h1>
      <h2>Get in touch for more details</h2>
    </Title>
    <Contact>
      <Box>
        <h2>Business</h2>
        <h3>business@herugunawan.tech</h3>
      </Box>
      <Box>
        <h2>Others</h2>
        <h3>info@herugunawan.tech</h3>
      </Box>
      <Box>
        <h2>Area</h2>
        <h3>89, Baskoro Raya, Tembalang</h3>
      </Box>
      <Box>
        <h2>Phone &amp; Chat</h2>
        <h3>+62 8954 1110 2315</h3>
      </Box>
      <Box>
        <h2>Social Media</h2>
        <div>
          <StyledLink
            href="https://twitter.com/bnhr07"
            target="_blank"
            rel="noopener noreferrer"
          >
            Twitter
          </StyledLink>
          <StyledLink
            href="https://t.me/bnhr7"
            target="_blank"
            rel="noopener noreferrer"
          >
            Telegram
          </StyledLink>
        </div>
      </Box>
      <Box>
        <h2>Dev Things</h2>
        <div>
          <StyledLink
            href="https://github.com/bnhr"
            target="_blank"
            rel="noopener noreferrer"
          >
            Github
          </StyledLink>
          <StyledLink
            href="https://gitlab.com/bnhr"
            target="_blank"
            rel="noopener noreferrer"
          >
            Gitlab
          </StyledLink>
        </div>
      </Box>
    </Contact>
  </Layout>
)

export default ContactPage
