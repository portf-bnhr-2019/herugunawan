import React from "react"

import { graphql, useStaticQuery } from "gatsby"
import Img from "gatsby-image"

// import astro from "../images/astronaut.png"

import MenuLink from "../components/theme/StyledLink"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Title from "../components/title/title"

import { device } from "../styles/device"

import styled from "styled-components"

const WorkContainer = styled.section`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(40rem, 1fr));
  grid-auto-rows: minmax(15rem, auto);
  grid-gap: 5rem;

  @media ${device.mobileL} {
    grid-template-columns: repeat(auto-fill, minmax(27rem, 1fr));
  }
  @media ${device.mobileM} {
    grid-template-columns: repeat(auto-fill, minmax(21rem, 1fr));
  }
  @media ${device.mobileS} {
    grid-template-columns: repeat(auto-fill, minmax(15rem, 1fr));
  }
`

const WorkList = styled.article`
  margin-bottom: calc(1.2rem + (30 - 12) * ((100vw - 30rem) / (1600 - 300)));
  img {
    max-height: 45rem;
    object-fit: cover;
    object-position: center;
    border-radius: 5px;

    @media ${device.mobileL} {
      max-height: 100%;
    }
    @media ${device.mobileM} {
      max-height: 100%;
    }
    @media ${device.mobileS} {
      max-height: 100%;
    }
  }
`

const WorkImage = styled.div`
  position: relative;
  overflow: hidden;
`

const WorkContent = styled.div`
  display: flex;
  padding-top: 3rem;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
`

const Cat = styled.div`
  text-transform: uppercase;
  letter-spacing: 0.3rem;
`

const TitleWork = styled.div`
  font-size: 2.2rem;
`

const WorkPage = () => {
  const data = useStaticQuery(graphql`
    query {
      allContentfulWork {
        edges {
          node {
            id
            slug
            title
            smallimg {
              title
              fluid(maxWidth: 800, resizingBehavior: SCALE) {
                ...GatsbyContentfulFluid
              }
            }
            categori {
              title
            }
          }
        }
      }
    }
  `)

  return (
    <Layout>
      <SEO
        title="See Everything I've Made"
        description="See and know better how Heru Gunawan work on a real life projects"
      />
      <Title>
        <h1>Works</h1>
        <h2>See Everything I've Made</h2>
      </Title>

      <WorkContainer>
        {data.allContentfulWork.edges.map(edge => {
          const shorten = edge.node
          return (
            <WorkList key={edge.node.id}>
              <MenuLink to={`/works/${shorten.slug}`}>
                <WorkImage>
                  <Img
                    fluid={shorten.smallimg.fluid}
                    alt={shorten.smallimg.title}
                  />
                </WorkImage>
              </MenuLink>
              <WorkContent>
                <Cat>{shorten.categori.title}</Cat>
                <MenuLink to={`/works/${shorten.slug}`}>
                  <TitleWork>{shorten.title}</TitleWork>
                </MenuLink>
              </WorkContent>
            </WorkList>
          )
        })}
      </WorkContainer>
    </Layout>
  )
}

export default WorkPage
