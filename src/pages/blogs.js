import React from "react"

import { graphql, useStaticQuery } from "gatsby"
import Img from "gatsby-image"

import { formatDistance } from "date-fns"

import MenuLink from "../components/theme/StyledLink"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Title from "../components/title/title"

import styled from "styled-components"
import style from "styled-theming"

import { device } from "../styles/device"

const shadowColor = style("mode", {
  light: "rgba(0, 0, 0, 0.1)",
  dark: "rgba(0, 0, 0, 0.1)",
})

const shadowColor2 = style("mode", {
  light: "rgba(0, 0, 0, 0.04)",
  dark: "rgba(0, 0, 0, 0.02)",
})

const BlogContainer = styled.div`
  margin-bottom: 10rem;
`

const BlogList = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 3rem;
  transition: all 0.5s ease;
  border-radius: 5px;
  box-shadow: 0 4px 6px -1px ${shadowColor}, 0 2px 4px -1px ${shadowColor2};

  &:hover {
    box-shadow: 0 10px 15px -3px ${shadowColor}, 0 4px 6px -2px ${shadowColor2};

    div img {
      filter: grayscale(0);
      transition: 0.5s ease;
    }

    @media ${device.mobileL} {
      box-shadow: none;
    }
    @media ${device.mobileM} {
      box-shadow: none;
    }
    @media ${device.mobileS} {
      box-shadow: none;
    }
  }

  @media ${device.mobileL} {
    flex-direction: column;
    box-shadow: none;
  }
  @media ${device.mobileM} {
    flex-direction: column;
    box-shadow: none;
  }
  @media ${device.mobileS} {
    flex-direction: column;
    box-shadow: none;
  }
`

const BlogImage = styled.div`
  width: 100%;
  max-width: 37.65rem;
  max-height: 21.1rem;
  position: relative;
  overflow: hidden;
  transition: 0.5s ease;
  overflow: hidden;

  img {
    object-fit: cover;
    object-position: center;
    filter: grayscale(1);
    transition: filter 0.5s ease;
    border-radius: 5px 0 0 5px;

    @media ${device.mobileL} {
      border-radius: 0;
    }

    @media ${device.mobileM} {
      border-radius: 0;
    }

    @media ${device.mobileS} {
      border-radius: 0;
    }
  }

  @media ${device.mobileL} {
    max-width: 100%;
  }
  @media ${device.mobileM} {
    max-width: 100%;
  }
  @media ${device.mobileS} {
    max-width: 100%;
  }
`

const BlogContent = styled.div`
  padding: 3.2rem;
  width: calc(100% - 20.52rem);
  background-color: transparent;

  @media ${device.mobileL} {
    width: 100%;
    padding: 3.2rem 0;
  }
  @media ${device.mobileM} {
    width: 100%;
    padding: 3.2rem 0;
  }
  @media ${device.mobileS} {
    width: 100%;
    padding: 3.2rem 0;
  }
`

const TitleBlog = styled.div`
  h3 {
    text-transform: capitalize;
    font-size: calc(1.8rem + (23 - 18) * ((100vw - 30rem) / (1600 - 300)));
    margin: 1rem 0;
    min-height: 8rem;

    @media ${device.mobileL} {
      min-height: 0rem;
    }
    @media ${device.mobileM} {
      min-height: 0rem;
    }
    @media ${device.mobileS} {
      min-height: 0rem;
    }
  }
`

const Additional = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;

  svg {
    margin-right: 1rem;
  }
  span {
    text-transform: uppercase;
  }
  time {
    text-transform: uppercase;
  }

  @media ${device.mobileL} {
    display: none;
  }
  @media ${device.mobileM} {
    display: none;
  }
  @media ${device.mobileS} {
    display: none;
  }
`

const MobileAdditional = styled.div`
  display: none;

  span {
    font-size: calc(1.5rem + (17 - 15) * ((100vw - 32rem) / (756 - 320)));
  }

  @media ${device.mobileL} {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  @media ${device.mobileM} {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  @media ${device.mobileS} {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`

const BlogPage = () => {
  const data = useStaticQuery(graphql`
    query {
      allContentfulBlog(sort: { fields: [date], order: DESC }) {
        edges {
          node {
            id
            title
            slug
            categori {
              title
            }
            date
            image {
              title
              fluid(maxWidth: 800, resizingBehavior: SCALE) {
                ...GatsbyContentfulFluid
              }
            }
          }
        }
      }
    }
  `)

  return (
    <Layout>
      <SEO
        title="From personal stories to tutorials"
        description="Blogs wrote by Heru to share like personal stories, tutorials, favorite things, and much more"
      />
      <Title>
        <h1>Blogs</h1>
        <h2>i write everything that comes to my mind</h2>
      </Title>

      <BlogContainer>
        {data.allContentfulBlog.edges.map(edge => {
          const shorten = edge.node
          return (
            <BlogList key={shorten.id}>
              <BlogImage>
                <MenuLink to={`/blogs/${shorten.slug}`}>
                  <Img fluid={shorten.image.fluid} alt={shorten.image.title} />
                </MenuLink>
              </BlogImage>
              <BlogContent>
                <MobileAdditional>
                  <span>{shorten.categori.title}</span>
                  <span>
                    {formatDistance(new Date(shorten.date), new Date(), {
                      addSuffix: true,
                    })}
                  </span>
                </MobileAdditional>
                <Additional>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-archive"
                  >
                    <polyline points="21 8 21 21 3 21 3 8"></polyline>
                    <rect x="1" y="3" width="22" height="5"></rect>
                    <line x1="10" y1="12" x2="14" y2="12"></line>
                  </svg>
                  <span>{shorten.categori.title}</span>
                </Additional>
                <TitleBlog>
                  <MenuLink to={`/blogs/${shorten.slug}`}>
                    <h3>{shorten.title}</h3>
                  </MenuLink>
                </TitleBlog>
                <Additional>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-calendar"
                  >
                    <rect
                      x="3"
                      y="4"
                      width="18"
                      height="18"
                      rx="2"
                      ry="2"
                    ></rect>
                    <line x1="16" y1="2" x2="16" y2="6"></line>
                    <line x1="8" y1="2" x2="8" y2="6"></line>
                    <line x1="3" y1="10" x2="21" y2="10"></line>
                  </svg>
                  <time>
                    {formatDistance(new Date(shorten.date), new Date(), {
                      addSuffix: true,
                    })}
                  </time>
                </Additional>
              </BlogContent>
            </BlogList>
          )
        })}
      </BlogContainer>
    </Layout>
  )
}

export default BlogPage
