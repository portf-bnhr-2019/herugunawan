import React from "react"

import { graphql } from "gatsby"

import { format } from "date-fns"

import Layout from "../components/layout"
import SEO from "../components/seo"

import InterLink from "../components/theme/InterLink"
import Title from "../components/theme/Title"
import Span from "../components/theme/Span"

import styled from "styled-components"
import style from "styled-theming"

import { device } from "../styles/device"

const bgColor = style("mode", {
  light: "#f5f5f5",
  dark: "#202d3b",
})

const Intro = styled.section`
  min-height: 30vh;
  height: 100%;

  h1 {
    font-size: calc(2rem + (50 - 20) * ((100vw - 30rem) / (1600 - 300)));
  }

  @media ${device.tablet} {
    min-height: 10vh;
  }

  @media ${device.mobileL} {
    min-height: 0;
  }

  @media ${device.mobileM} {
    min-height: 0;
  }

  @media ${device.mobileS} {
    min-height: 0;
  }
`

const Section = styled.section`
  margin: 4rem 0;
  padding: 4rem 0;

  a {
    font-size: calc(1.7rem + (21 - 17) * ((100vw - 30rem) / (1600 - 300)));
  }
`

const BlogContainer = styled.div`
  margin-bottom: 4rem;
`

const BlogList = styled.article`
  display: flex;
  border: 2px solid transparent;
  margin-bottom: 2rem;
  padding: 2rem;
  transition: background-color 0.2s ease-in;

  &:hover {
    border: 2px solid transparent;
    background-color: ${bgColor};
  }

  &:last-child {
    margin-bottom: 0rem;
  }

  @media ${device.mobileL} {
    flex-direction: column;
  }

  @media ${device.mobileM} {
    flex-direction: column;
  }

  @media ${device.mobileS} {
    flex-direction: column;
  }
`

const BlogDate = styled.div`
  position: relative;
  margin-right: 4.5rem;
  height: 10rem;
  width: 10rem;
  overflow: hidden;
  flex-shrink: 0;

  @media ${device.mobileL} {
    margin: 0 auto;
  }

  @media ${device.mobileM} {
    margin: 0 auto;
  }

  @media ${device.mobileS} {
    margin: 0 auto;
  }
`

const BlogDateInner = styled.div`
  text-align: center;
  width: 100%;
  height: 100%;
  line-height: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: calc(3rem + (50 - 30) * ((100vw - 30rem) / (1600 - 300)));
  font-weight: 700;

  span {
    font-size: 2rem;
    text-transform: uppercase;
    display: block;
    margin-top: 1.2rem;
    font-weight: 400;
  }
`

const BlogContent = styled.div`
  align-self: center;

  span {
    font-size: 1.7rem;
  }

  a {
    margin-top: 1.2rem;

    h4 {
      font-size: calc(1.7rem + (22 - 17) * ((100vw - 30rem) / (1600 - 300)));
    }
  }

  @media ${device.mobileL} {
    align-self: flex-start;
  }

  @media ${device.mobileM} {
    align-self: flex-start;
  }

  @media ${device.mobileS} {
    align-self: flex-start;
  }
`

const CenterButton = styled.div`
  text-align: center;
  margin-top: 2rem;
`

const WorkContainer = styled.section`
  border: 0;
`

const WorkList = styled.div`
  border: 0;
`

const WorkItems = styled.div`
  font-size: calc(2rem + (97 - 20) * ((100vw - 30rem) / (1600 - 300)));
  line-height: 1.1em;
  font-weight: bold;
  letter-spacing: 2px;
  margin-bottom: calc(2rem + (50 - 20) * ((100vw - 30rem) / (1600 - 300)));
`

const IndexPage = ({ data }) => {
  const works = data.work
  const blogs = data.blog

  return (
    <Layout>
      <SEO
        title="Meet your Front End Developer Partner"
        description="Your Front End Developer Partner who will help you to increase your business"
      />

      <Intro>
        <h1>
          <InterLink to="/about">heru gunawan</InterLink> is front end web
          developer based on semarang, with two years of experience in creating
          a multipurpose website for various fields
        </h1>
      </Intro>

      <Section>
        <Title>Selected Works :</Title>
        <WorkContainer>
          {works.edges.map(edge => {
            return (
              <WorkList key={edge.node.id}>
                <InterLink to={`/works/${edge.node.slug}`}>
                  <WorkItems>{edge.node.title}</WorkItems>
                </InterLink>{" "}
              </WorkList>
            )
          })}
        </WorkContainer>

        <CenterButton>
          <InterLink to="/works">
            See more works <span>&rarr;</span>{" "}
          </InterLink>{" "}
        </CenterButton>
      </Section>

      <Section>
        <Title>Latest Blogs :</Title>
        <BlogContainer>
          {blogs.edges.map(edge => {
            return (
              <BlogList key={edge.node.id}>
                <BlogDate>
                  <BlogDateInner>
                    {format(new Date(edge.node.date), "dd")}{" "}
                    <Span>{format(new Date(edge.node.date), "MMM")}</Span>
                  </BlogDateInner>
                </BlogDate>
                <BlogContent>
                  <Span>
                    <span>#{edge.node.categori.title}</span>
                  </Span>
                  <InterLink to={`/blogs/${edge.node.slug}`}>
                    <h4>{edge.node.title}</h4>
                  </InterLink>{" "}
                </BlogContent>
              </BlogList>
            )
          })}
        </BlogContainer>

        <CenterButton>
          <InterLink to="/blogs">
            Read more blogs <span>&rarr;</span>{" "}
          </InterLink>{" "}
        </CenterButton>
      </Section>
    </Layout>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query {
    blog: allContentfulBlog(sort: { fields: [date], order: DESC }, limit: 3) {
      edges {
        node {
          id
          title
          slug
          date
          categori {
            title
          }
        }
      }
    }
    work: allContentfulWork(
      sort: { fields: [createdAt], order: DESC }
      limit: 3
    ) {
      edges {
        node {
          id
          title
          slug
          categori {
            title
          }
        }
      }
    }
  }
`
