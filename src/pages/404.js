import React from "react"

import SEO from "../components/seo"

import Error from "../../static/images/error.svg"

import GlobalStyle from "../styles/globalStyles"

import styled from "styled-components"

import { device } from "../styles/device"

import InterLink from "../components/theme/InterLink"

const Alien = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 100vh;

  svg {
    max-width: 60rem;
    width: 100%;
    height: auto;
  }

  p {
    margin-top: 3rem;
    font-size: calc(1.8rem + (25 - 18) * ((100vw - 30rem) / (1600 - 300)));

    @media ${device.mobileL} {
      padding: 0 2rem;
    }

    @media ${device.mobileM} {
      padding: 0 2rem;
    }

    @media ${device.mobileS} {
      padding: 0 2rem;
    }
  }

  span {
    margin-top: 3rem;
    font-size: calc(1.8rem + (25 - 18) * ((100vw - 30rem) / (1600 - 300)));
  }
`

const NotFoundPage = () => (
  <>
    <SEO
      title="404: Not found"
      description="Lost in in the middle of nowhere, links or things you are looking for in not exist or deleted"
    />

    <Alien>
      <GlobalStyle />
      <Error />
      <p>The page or things you're looking for is taken by aliens...</p>
      <span>
        Go back to <InterLink to="/">Home</InterLink> before alien take you
      </span>
    </Alien>
  </>
)

export default NotFoundPage
